package kvpaxos

import "log"
import "net/rpc"
import "fmt"

const Debug = 0

func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		log.Printf(format, a...)
	}
	return
}

const (
	OK       = "OK"
	ErrNoKey = "ErrNoKey"
	PutOp = "Put"
	AppendOp = "Append"
	GetOp = "Get"
)

type ClientXid struct {
	ClientId int64
	Xid int
}

type Err string

// Put or Append
type PutAppendArgs struct {
    Xid ClientXid
    Op    string // "Put" or "Append"
	Key   string
	Value string
    HighestGet int
}

type PutAppendReply struct {
	Err Err
}

type GetArgs struct {
    Xid ClientXid
	Key string
    HighestGet int
}

type GetReply struct {
	Err   Err
	Value string
    Seq int
}

//
// call() sends an RPC to the rpcname handler on server srv
// with arguments args, waits for the reply, and leaves the
// reply in reply. the reply argument should be a pointer
// to a reply structure.
//
// the return value is true if the server responded, and false
// if call() was not able to contact the server. in particular,
// the reply's contents are only valid if call() returned true.
//
// you should assume that call() will return an
// error after a while if the server is dead.
// don't provide your own time-out mechanism.
//
// please use call() to send all RPCs, in client.go and server.go.
// please don't change this function.
//
func call(srv string, rpcname string,
	args interface{}, reply interface{}) bool {
	c, errx := rpc.Dial("unix", srv)
	if errx != nil {
		return false
	}
	defer c.Close()

	err := c.Call(rpcname, args, reply)
	if err == nil {
		return true
	}

	fmt.Println(err)
	return false
}
