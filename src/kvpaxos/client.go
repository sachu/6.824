package kvpaxos

import "crypto/rand"
import "math/big"
import "time"
import "sync"
//import "fmt"

type Clerk struct {
	servers []string
	clientId int64
	xid int
    mu sync.Mutex
    // We cache the get values on the server side.
    // The client will send its highest get sequence
    // number it's received to the server so the
    // server can remove this value.
    highestGet int
}

func nrand() int64 {
	max := big.NewInt(int64(1) << 62)
	bigx, _ := rand.Int(rand.Reader, max)
	x := bigx.Int64()
	return x
}

func MakeClerk(servers []string) *Clerk {
	ck := new(Clerk)
	ck.servers = servers
	ck.clientId = nrand()
	ck.xid = 0
    ck.highestGet = -1
	return ck
}

//
// fetch the current value for a key.
// returns "" if the key does not exist.
// keeps trying forever in the face of all other errors.
//
func (ck *Clerk) Get(key string) string {
    ck.mu.Lock()
    xid := ck.xid
    ck.xid += 1
    ck.mu.Unlock()
    
    args := &GetArgs{}
    args.Xid = ClientXid{ClientId: ck.clientId, Xid: xid}
    args.Key = key
    args.HighestGet = ck.highestGet

    attempt := 1
    numServers := len(ck.servers)
    idx := 0

    for {
        DPrintf("GET (attempt %d) args: %s", attempt, args)
        var reply GetReply
        ok := call(ck.servers[idx], "KVPaxos.Get", args, &reply)
        if ok {
            ck.mu.Lock()
            if reply.Seq > ck.highestGet {
                ck.highestGet = reply.Seq
            }
            ck.mu.Unlock()
            return reply.Value
        }
        idx = (idx + 1) % numServers
        time.Sleep(time.Millisecond * 100)
        attempt++
    }
	return ""
}

//
// shared by Put and Append.
//
func (ck *Clerk) PutAppend(key string, value string, op string) {
    ck.mu.Lock()
    xid := ck.xid
    ck.xid += 1
    ck.mu.Unlock()
    
    args := &PutAppendArgs{}
    args.Xid = ClientXid{ClientId: ck.clientId, Xid: xid}
    args.Op = op
    args.Key = key
    args.Value = value
    args.HighestGet = ck.highestGet
    
    attempt := 1
    numServers := len(ck.servers)
    idx := 0

    for {
        DPrintf("PUT (attempt %d) args: %s", attempt, args)
        var reply PutAppendReply
        ok := call(ck.servers[idx], "KVPaxos.PutAppend", args, &reply)
        if ok {
            return
        }
        idx = (idx + 1) % numServers
        time.Sleep(time.Millisecond * 100)
        attempt++
    }
}

func (ck *Clerk) Put(key string, value string) {
	ck.PutAppend(key, value, "Put")
}
func (ck *Clerk) Append(key string, value string) {
	ck.PutAppend(key, value, "Append")
}
