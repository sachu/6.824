package kvpaxos

import "log"
import "net"
import "fmt"
import "net/rpc"
import "paxos"
import "sync"
import "sync/atomic"
import "os"
import "syscall"
import "encoding/gob"
import "math/rand"
import "time"
import "math"


type Op struct {
	Me int // identifies server
	Xid ClientXid // identifies specific client request
	Op string // Get/Put/Append
	Key string
	Val string
}

type KVPaxos struct {
	mu         sync.Mutex
	l          net.Listener
	me         int
	dead       int32 // for testing
	unreliable int32 // for testing
	px         *paxos.Paxos
    /*
    We update state sequentially. seq is the next instance update
    to this server's local state.
    state reflects state up to instance number (seq-1).
    */
	seq int
	state map[string]string

    seenXid map[ClientXid]bool
    getcache map[int]interface{} // We cache Get values by sequence number.
    getmap map[ClientXid]int
    // maps outstanding Get calls' clientId (int64) to (xid:seq) pairs
    outstandingGet map[int64]map[int]int
}

func (kv *KVPaxos) updateState(seq int, op Op) interface{} {
	kv.mu.Lock()
	defer kv.mu.Unlock()
    
    var opValue interface{} = nil

	if seq != kv.seq {
        // We have already applied this sequence log,
        // so we shouldn't reapply. This can happen
        // with multiple concurrent requests to the server.
        if op.Op == GetOp {
            cachedGet, ok := kv.getcache[seq]
            if ok {
                opValue = cachedGet
            } else {
                // This happens when a server is catching up.
                opValue = ""
            }
        }
        return opValue
    }

    // The passed in sequence number is the next operation
    // used to update the local state (kv.state)

    if (op.Op == GetOp) {
        val, ok := kv.state[op.Key]
        if ok {
            opValue = val
        } else {
            // No key
            opValue = ""
        }
        kv.getcache[seq] = opValue
        kv.getmap[op.Xid] = seq

    } else if (op.Op == PutOp) {
        kv.state[op.Key] = op.Val

    } else if (op.Op == AppendOp) {
        newVal := op.Val
        val, ok := kv.state[op.Key]
        if ok {
            newVal = val + op.Val
        }
        kv.state[op.Key] = newVal

    } else {
        log.Fatalf("Unrecognized op in update state. op=%s", op)
    }

    // Advance kv.seq
    kv.seq += 1

    kv.seenXid[op.Xid] = true
    // We're done with all instances <= seq.
    // We know this because we increment kv.seq only in this function.
    kv.px.Done(seq)
    return opValue
}

func (kv *KVPaxos) cleanCachedGets(seq int) {
    kv.mu.Lock()
    defer kv.mu.Unlock()
    
    lowestGetSeq := math.MaxInt32

    for _ , xidToSeq := range kv.outstandingGet {
        for _, getSeq := range xidToSeq {
            if getSeq < lowestGetSeq {
                lowestGetSeq = getSeq
            }
        }
    }

    for sequence, _ := range kv.getcache {
        if sequence < seq && sequence < lowestGetSeq {
            delete(kv.getcache, sequence)
        }
    }
}


func (kv *KVPaxos) Get(args *GetArgs, reply *GetReply) error {
    kv.cleanCachedGets(args.HighestGet)

	var op Op = Op{}
	op.Me = kv.me
	op.Xid = args.Xid
	op.Op = GetOp
	op.Key = args.Key
    
    seq := kv.seq
    for {
        kv.mu.Lock()
        _, found := kv.seenXid[op.Xid]
        if !found {
            kv.px.Start(seq, op)

        } else {
            reply.Seq, _ = kv.getmap[op.Xid]
            thegetvalue, ok := kv.getcache[reply.Seq]
            if ok {
              reply.Value = thegetvalue.(string)
            }
            reply.Err = OK
            kv.mu.Unlock()
            return nil
        }
        
        _, found = kv.outstandingGet[op.Xid.ClientId]
        if !found {
            kv.outstandingGet[op.Xid.ClientId] = make(map[int]int)
        }
        kv.outstandingGet[op.Xid.ClientId][op.Xid.Xid] = seq
        kv.mu.Unlock()

        // wait until seq has been decided
    	to := 10 * time.Millisecond
    	for {
    		status, decidedVal := kv.px.Status(seq)
            DPrintf("me: %d, seq: %d, op: %s\n   status: %s, decision: %s",
                    kv.me, seq, op, status, decidedVal)
            if status == paxos.Pending {
                // Do nothing. Will sleep again.

            } else if status == paxos.Forgotten {
                // Need to fast forward. This situation can happen
                // if this thread was interrupted and another thread
                // had processed agreements. Then when we context switch
                // back to this thread, the status might have been forgotten.
                DPrintf("Tried to find status of a forgotten instance")
                break
                
            } else if status == paxos.Decided {
                decidedOp := decidedVal.(Op)
    			getValue := kv.updateState(seq, decidedOp)
    			if decidedOp.Xid == op.Xid {
                    // This request has been committed, though
                    // not necessarily by this server.
                    reply.Value = getValue.(string)
                    reply.Err = OK
                    reply.Seq = seq
                    return nil

    			} else {
                    // This request's Xid was not committed, so we will
                    // reset seq and re-Start().
                    break
    			}
        	}
            
            // seq instance not decided, so sleep
        	time.Sleep(to)
        	if to < 10 * time.Second {
       		   to *= 2
        	}
      	}

        seq = kv.seq // Will call Start() on the re-loop
    }
	
	return nil
}

/*
    PUT/APPEND
*/
func (kv *KVPaxos) PutAppend(args *PutAppendArgs, reply *PutAppendReply) error {
    kv.cleanCachedGets(args.HighestGet)

    var op Op = Op{}
	op.Me = kv.me
	op.Xid = args.Xid
	op.Op = args.Op
	op.Key = args.Key
    op.Val = args.Value
    
    seq := kv.seq
    for {
        kv.mu.Lock()
        _, found := kv.seenXid[op.Xid]
        if !found {
            kv.px.Start(seq, op)

        } else {
            kv.mu.Unlock()
            return nil
        }
        kv.mu.Unlock()

        // wait until seq has been decided
    	to := 10 * time.Millisecond
    	for {
    		status, decidedVal := kv.px.Status(seq)
            DPrintf("me: %d, seq: %d, op: %s\n   status: %s, decision: %s",
                    kv.me, seq, op, status, decidedVal)
            if status == paxos.Pending {
                // Do nothing. Will sleep and then call Status again.
                
            } else if status == paxos.Forgotten {
                // This thread needs to fast-forward its sequence number.
                break
                
            } else if status == paxos.Decided {
                decidedOp := decidedVal.(Op)
    			kv.updateState(seq, decidedOp)
    			if decidedOp.Xid == op.Xid {
                    // This request has been committed, but not necessarily by this server.
                    reply.Err = OK
                    return nil
    			} else {
                    // This request's Xid was not committed, so re-loop.
                    break
    			}
        	}
            
            // Instance not decided, so sleep and re-check.
        	time.Sleep(to)
        	if to < 10 * time.Second {
       		   to *= 2
        	}
      	}

        seq = kv.seq // Will call Start() on the re-loop, so assign seq to an up-to-date value
    }
	
	return nil
}

// tell the server to shut itself down.
// please do not change these two functions.
func (kv *KVPaxos) kill() {
	DPrintf("Kill(%d): die\n", kv.me)
	atomic.StoreInt32(&kv.dead, 1)
	kv.l.Close()
	kv.px.Kill()
}

// call this to find out if the server is dead.
func (kv *KVPaxos) isdead() bool {
	return atomic.LoadInt32(&kv.dead) != 0
}

// please do not change these two functions.
func (kv *KVPaxos) setunreliable(what bool) {
	if what {
		atomic.StoreInt32(&kv.unreliable, 1)
	} else {
		atomic.StoreInt32(&kv.unreliable, 0)
	}
}

func (kv *KVPaxos) isunreliable() bool {
	return atomic.LoadInt32(&kv.unreliable) != 0
}

//
// servers[] contains the ports of the set of
// servers that will cooperate via Paxos to
// form the fault-tolerant key/value service.
// me is the index of the current server in servers[].
//
func StartServer(servers []string, me int) *KVPaxos {
	// call gob.Register on structures you want
	// Go's RPC library to marshall/unmarshall.
	gob.Register(Op{})

	kv := new(KVPaxos)
	kv.me = me
	kv.seq = 0
	kv.state = make(map[string]string)
    kv.getcache = make(map[int]interface{})
    kv.getmap = make(map[ClientXid]int)
    kv.seenXid = make(map[ClientXid]bool)
    kv.outstandingGet = make(map[int64]map[int]int)

	rpcs := rpc.NewServer()
	rpcs.Register(kv)

	kv.px = paxos.Make(servers, me, rpcs)

	os.Remove(servers[me])
	l, e := net.Listen("unix", servers[me])
	if e != nil {
		log.Fatal("listen error: ", e)
	}
	kv.l = l


	// please do not change any of the following code,
	// or do anything to subvert it.

	go func() {
		for kv.isdead() == false {
			conn, err := kv.l.Accept()
			if err == nil && kv.isdead() == false {
				if kv.isunreliable() && (rand.Int63()%1000) < 100 {
					// discard the request.
					conn.Close()
				} else if kv.isunreliable() && (rand.Int63()%1000) < 200 {
					// process the request but force discard of reply.
					c1 := conn.(*net.UnixConn)
					f, _ := c1.File()
					err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
					if err != nil {
						fmt.Printf("shutdown: %v\n", err)
					}
					go rpcs.ServeConn(conn)
				} else {
					go rpcs.ServeConn(conn)
				}
			} else if err == nil {
				conn.Close()
			}
			if err != nil && kv.isdead() == false {
				fmt.Printf("KVPaxos(%v) accept: %v\n", me, err.Error())
				kv.kill()
			}
		}
	}()

	return kv
}
