package shardmaster

import "net"
import "fmt"
import "net/rpc"
import "log"

import "paxos"
import "sync"
import "sync/atomic"
import "os"
import "syscall"
import "encoding/gob"
import "math/rand"
import "time"

var Debug int = 0

func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		log.Printf(format, a...)
	}
	return
}

const (
	Join = "Join"
    Leave = "Leave"
    Move = "Move"
    Query = "Query"
)

type ShardMaster struct {
	mu         sync.Mutex
	l          net.Listener
	me         int
	dead       int32 // for testing
	unreliable int32 // for testing
	px         *paxos.Paxos
    seq int // next paxos instance to agree upon
	configs []Config // indexed by config num
}

type Op struct {
	Op string // Join/Leave/Move/Query
    GID int64 // unique group replica ID
    Servers []string // group server ports
    Shard int
    Xid int64
    Num int // Config number requested by Query
}

func nextConfig(orig Config) Config {
    cp := Config{}
    cp.Num = orig.Num + 1
    cp.Shards = orig.Shards
    cp.Groups = make(map[int64][]string)
    for gid, shards := range orig.Groups {
        cp.Groups[gid] = shards
    }
    return cp
}

func rebalance(conf *Config, gidLeaving int64) {
    // if no groups, set all shards to zero
    if len(conf.Groups) == 0 {
        conf.Shards = [NShards]int64{}
    }
    
    // base case. shards are assigned the invalid group 0.
    // assign all shards to the existing gid
    if conf.Shards[0] == 0 {
        for gid, _ := range conf.Groups {
            for shardNum, _ := range conf.Shards {
                conf.Shards[shardNum] = gid
            }
        }
        // (there is only 1 gid at this time)
        return
    }
    
    // do we have a gid leaving?
    if gidLeaving > 0 {
        groupsToNumShards := make(map[int64]int)
        for gid, _ := range conf.Groups {
            groupsToNumShards[gid] = 0
        }
        for _, gid := range conf.Shards {
            groupsToNumShards[gid] += 1
        }
        delete(groupsToNumShards, gidLeaving)
        // assign these shards initially to the smallest groups
        for idx, gid := range conf.Shards {
            if gid == gidLeaving {
                smallGid := getGroupWithLeastShards(groupsToNumShards)
                conf.Shards[idx] = smallGid
                groupsToNumShards[smallGid]++
            }
        }
    }
    
    min := NShards / len(conf.Groups)
    moreThanMin := NShards % len(conf.Groups) // number of groups that will have min+1 shards
    
    // Tally up the group numbers.
    groupsToNumShards := make(map[int64]int)
    for gid, _ := range conf.Groups {
        groupsToNumShards[gid] = 0
    }
    for _, gid := range conf.Shards {
        groupsToNumShards[gid] += 1
    }
    
    // Collect all groups that are below min
    belowMin := make(map[int64]int)
    for gid, numShards := range groupsToNumShards {
        if numShards < min {
            belowMin[gid] = numShards
        }
    }
    
    // Collect groups that are above min.
    aboveMin := make(map[int64]int)
    for gid, numShards := range groupsToNumShards {
        if numShards > min {
            // Remove from the above min pool groups that have min + 1 shards
            // up to moreThanMin times.
            if numShards == (min + 1) && moreThanMin > 0 {
                moreThanMin--
                continue
            } else {
                aboveMin[gid] = numShards
            }
        }
    }
    
    // Balance between belowMin and aboveMin
    for {
        bigGid := getGroupWithMostShards(aboveMin)
        smallGid := getGroupWithLeastShards(belowMin)
        if (aboveMin[bigGid] - belowMin[smallGid]) <= 1 {
            break
        }
        // transfer one of big to small
        for idx, gid := range conf.Shards {
            if gid == bigGid {
                conf.Shards[idx] = smallGid
                aboveMin[bigGid]--
                belowMin[smallGid]++
                break
            }
        }
    }
    
}



func getGroupWithMostShards(gidToShards map[int64]int) int64 {
    var bigGid int64
    mostShards := -1
    for gid, numShards := range gidToShards {
        if numShards > mostShards {
            bigGid = gid
            mostShards = numShards
        }
    }
    return bigGid
}

func getGroupWithLeastShards(gidToShards map[int64]int) int64 {
    var smallGid int64
    leastShards := NShards + 1
    for gid, numShards := range gidToShards {
        if numShards < leastShards {
            smallGid = gid
            leastShards = numShards
        }
    }
    return smallGid
}

func (sm *ShardMaster) updateState(seq int, op Op, xid int64) bool {
    // true if xid has been used to update local state by the
    // end of the function
    xidApplied := xid == op.Xid

	if seq != sm.seq {
        // We have already applied this sequence log,
        // so we shouldn't reapply. This can happen
        // with multiple concurrent requests to the server.
        //
        // TODO do we have to return cached query response?
        // I think not because we don't handle unreliable/repeated client requests.
        return xidApplied
    }

    // The passed in sequence number is the next operation
    // used to update the local state

    if (op.Op == Join) {
        // make copy of current config
        next := nextConfig(sm.configs[len(sm.configs)-1])
        next.Groups[op.GID] = op.Servers
        rebalance(&next, -1)
        sm.configs = append(sm.configs, next)
    } else if op.Op == Leave {
        next := nextConfig(sm.configs[len(sm.configs)-1])
        delete(next.Groups, op.GID)
        rebalance(&next, op.GID)
        sm.configs = append(sm.configs, next)
    } else if op.Op == Move {
        next := nextConfig(sm.configs[len(sm.configs)-1])
        next.Shards[op.Shard] = op.GID
        sm.configs = append(sm.configs, next)
        
    } else if op.Op == Query {
        
        
    } else {
        log.Fatalf("Unrecognized op in update state. op=%s", op)
    }

    sm.seq += 1
    // We're done with all instances <= seq.
    // We know this because we increment kv.seq only in this function.
    sm.px.Done(seq)
    return xidApplied
}


func (sm *ShardMaster) Join(args *JoinArgs, reply *JoinReply) error {
    sm.mu.Lock()
    defer sm.mu.Unlock()

    var op Op = Op{}
	op.Op = Join
    op.GID = args.GID
    op.Servers = args.Servers
    op.Xid = nrand()

    seq := sm.seq
    for {
        sm.px.Start(seq, op)
        // wait until seq has been decided
    	to := 10 * time.Millisecond
    	for {
    		status, decidedVal := sm.px.Status(seq)
            DPrintf("me: %d, seq: %d, op: %s\n   status: %s, decision: %s",
                    sm.me, seq, op, status, decidedVal)
            if status == paxos.Pending {
                // Do nothing. Will sleep and then call Status again.

            } else if status == paxos.Forgotten {
                // This thread needs to fast-forward its sequence number.
                break

            } else if status == paxos.Decided {
                decidedOp := decidedVal.(Op)
                applied := sm.updateState(seq, decidedOp, op.Xid)
                if applied {
                    return nil
    			} else {
                    // This request's Xid was not committed, so re-loop.
                    break
    			}
        	}
            
            // Instance not decided, so sleep and re-check.
        	time.Sleep(to)
        	if to < 10 * time.Second {
       		   to *= 2
        	}
      	}

        seq = sm.seq // Will call Start() on the re-loop, so assign seq to an up-to-date value
    }
    
	return nil
}

func (sm *ShardMaster) Leave(args *LeaveArgs, reply *LeaveReply) error {
	sm.mu.Lock()
    defer sm.mu.Unlock()

    var op Op = Op{}
	op.Op = Leave
    op.GID = args.GID
    op.Xid = nrand()

    seq := sm.seq
    for {
        sm.px.Start(seq, op)
        // wait until seq has been decided
    	to := 10 * time.Millisecond
    	for {
    		status, decidedVal := sm.px.Status(seq)
            DPrintf("me: %d, seq: %d, op: %s\n   status: %s, decision: %s",
                    sm.me, seq, op, status, decidedVal)
            if status == paxos.Pending {
                // Do nothing. Will sleep and then call Status again.

            } else if status == paxos.Forgotten {
                // This thread needs to fast-forward its sequence number.
                break

            } else if status == paxos.Decided {
                decidedOp := decidedVal.(Op)
                applied := sm.updateState(seq, decidedOp, op.Xid)
                if applied {
                    return nil
    			} else {
                    // This request's Xid was not committed, so re-loop.
                    break
    			}
        	}
            
            // Instance not decided, so sleep and re-check.
        	time.Sleep(to)
        	if to < 10 * time.Second {
       		   to *= 2
        	}
      	}

        seq = sm.seq // Will call Start() on the re-loop, so assign seq to an up-to-date value
    }
    
	return nil
}

func (sm *ShardMaster) Move(args *MoveArgs, reply *MoveReply) error {
	sm.mu.Lock()
    defer sm.mu.Unlock()

    var op Op = Op{}
	op.Op = Move
    op.Shard = args.Shard
    op.GID = args.GID
    op.Xid = nrand()

    seq := sm.seq
    for {
        sm.px.Start(seq, op)
        // wait until seq has been decided
    	to := 10 * time.Millisecond
    	for {
    		status, decidedVal := sm.px.Status(seq)
            DPrintf("me: %d, seq: %d, op: %s\n   status: %s, decision: %s",
                    sm.me, seq, op, status, decidedVal)
            if status == paxos.Pending {
                // Do nothing. Will sleep and then call Status again.

            } else if status == paxos.Forgotten {
                // This thread needs to fast-forward its sequence number.
                break

            } else if status == paxos.Decided {
                decidedOp := decidedVal.(Op)
                applied := sm.updateState(seq, decidedOp, op.Xid)
                if applied {
                    return nil
    			} else {
                    // This request's Xid was not committed, so re-loop.
                    break
    			}
        	}
            
            // Instance not decided, so sleep and re-check.
        	time.Sleep(to)
        	if to < 10 * time.Second {
       		   to *= 2
        	}
      	}

        seq = sm.seq // Will call Start() on the re-loop, so assign seq to an up-to-date value
    }
    
	return nil
}

func (sm *ShardMaster) Query(args *QueryArgs, reply *QueryReply) error {
	sm.mu.Lock()
    defer sm.mu.Unlock()

    var op Op = Op{}
	op.Op = Query
    op.Num = args.Num
    op.Xid = nrand()

    seq := sm.seq
    for {
        sm.px.Start(seq, op)
        // wait until seq has been decided
    	to := 10 * time.Millisecond
    	for {
    		status, decidedVal := sm.px.Status(seq)
            DPrintf("me: %d, seq: %d, op: %s\n   status: %s, decision: %s",
                    sm.me, seq, op, status, decidedVal)
            if status == paxos.Pending {
                // Do nothing. Will sleep and then call Status again.

            } else if status == paxos.Forgotten {
                // This thread needs to fast-forward its sequence number.
                break

            } else if status == paxos.Decided {
                decidedOp := decidedVal.(Op)
                applied := sm.updateState(seq, decidedOp, op.Xid)
                if applied {
                    if args.Num < 0 || args.Num >= len(sm.configs) {
                        reply.Config = sm.configs[len(sm.configs) - 1]
                    } else {
                        reply.Config = sm.configs[args.Num]
                    }
                    return nil
    			} else {
                    // This request's Xid was not committed, so re-loop.
                    break
    			}
        	}
            
            // Instance not decided, so sleep and re-check.
        	time.Sleep(to)
        	if to < 10 * time.Second {
       		   to *= 2
        	}
      	}

        seq = sm.seq // Will call Start() on the re-loop, so assign seq to an up-to-date value
    }
    
	return nil
}

// please don't change these two functions.
func (sm *ShardMaster) Kill() {
	atomic.StoreInt32(&sm.dead, 1)
	sm.l.Close()
	sm.px.Kill()
}

// call this to find out if the server is dead.
func (sm *ShardMaster) isdead() bool {
	return atomic.LoadInt32(&sm.dead) != 0
}

// please do not change these two functions.
func (sm *ShardMaster) setunreliable(what bool) {
	if what {
		atomic.StoreInt32(&sm.unreliable, 1)
	} else {
		atomic.StoreInt32(&sm.unreliable, 0)
	}
}

func (sm *ShardMaster) isunreliable() bool {
	return atomic.LoadInt32(&sm.unreliable) != 0
}

//
// servers[] contains the ports of the set of
// servers that will cooperate via Paxos to
// form the fault-tolerant shardmaster service.
// me is the index of the current server in servers[].
//
func StartServer(servers []string, me int) *ShardMaster {
	sm := new(ShardMaster)
	sm.me = me

	sm.configs = make([]Config, 1)
	sm.configs[0].Groups = map[int64][]string{}
    sm.seq = 0

	rpcs := rpc.NewServer()

	gob.Register(Op{})
	rpcs.Register(sm)
	sm.px = paxos.Make(servers, me, rpcs)

	os.Remove(servers[me])
	l, e := net.Listen("unix", servers[me])
	if e != nil {
		log.Fatal("listen error: ", e)
	}
	sm.l = l

	// please do not change any of the following code,
	// or do anything to subvert it.

	go func() {
		for sm.isdead() == false {
			conn, err := sm.l.Accept()
			if err == nil && sm.isdead() == false {
				if sm.isunreliable() && (rand.Int63()%1000) < 100 {
					// discard the request.
					conn.Close()
				} else if sm.isunreliable() && (rand.Int63()%1000) < 200 {
					// process the request but force discard of reply.
					c1 := conn.(*net.UnixConn)
					f, _ := c1.File()
					err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
					if err != nil {
						fmt.Printf("shutdown: %v\n", err)
					}
					go rpcs.ServeConn(conn)
				} else {
					go rpcs.ServeConn(conn)
				}
			} else if err == nil {
				conn.Close()
			}
			if err != nil && sm.isdead() == false {
				fmt.Printf("ShardMaster(%v) accept: %v\n", me, err.Error())
				sm.Kill()
			}
		}
	}()

	return sm
}
