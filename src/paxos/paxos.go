package paxos

//
// Paxos library, to be included in an application.
// Multiple applications will run, each including
// a Paxos peer.
//
// Manages a sequence of agreed-on values.
// The set of peers is fixed.
// Copes with network failures (partition, msg loss, &c).
// Does not store anything persistently, so cannot handle crash+restart.
//
// The application interface:
//
// px = paxos.Make(peers []string, me string)
// px.Start(seq int, v interface{}) -- start agreement on new instance
// px.Status(seq int) (Fate, v interface{}) -- get info about an instance
// px.Done(seq int) -- ok to forget all instances <= seq
// px.Max() int -- highest instance seq known, or -1
// px.Min() int -- instances before this seq have been forgotten
//

import "net"
import "net/rpc"
import "log"

import "os"
import "syscall"
import "sync"
import "sync/atomic"
import "fmt"
import "math/rand"

var debug_ bool = false

func debug(a ...interface{}) {
	if debug_ {
		fmt.Println(a)
	}
}

// px.Status() return values, indicating
// whether an agreement has been decided,
// or Paxos has not yet reached agreement,
// or it was agreed but forgotten (i.e. < Min()).
type Fate int

const (
	Decided   Fate = iota + 1
	Pending        // not yet decided.
	Forgotten      // decided but forgotten.
)

// proposal number
// to ensure uniqueness (so each pair has a disparate set
// of proposal number candidates) we use the peer's address (index in this case)
type PNum struct {
	Num		int
	Me		int
}

func comparePNum(pnum1 PNum, pnum2 PNum) int {
	if pnum1.Num < pnum2.Num {
		return -1
	}
	if pnum1.Num > pnum2.Num {
		return 1
	}
	if pnum1.Me < pnum2.Me {
		return -1
	}
	if pnum1.Me > pnum2.Me {
		return 1
	}
	return 0
}

type Instance struct {
	n_p			PNum // highest prepare seen
	n_a			PNum // highest accept seen
    v_a			interface{} // value of highest accept seen
    fate		Fate
    decidedVal	interface{} // decided value, only valid if fate == Decided
}

func initInstance() *Instance {
	newInstance := &Instance{}
	newInstance.n_p = PNum{Num: -1, Me: -1}
	newInstance.n_a = PNum{Num: -1, Me: -1}
	newInstance.v_a = nil
	newInstance.fate = Pending
	newInstance.decidedVal = nil
	return newInstance
}

type Paxos struct {
	mu         sync.Mutex
	l          net.Listener
	dead       int32 // for testing
	unreliable int32 // for testing
	rpcCount   int32 // for testing
	peers      []string
	me         int // index into peers[]
	instances	map[int]*Instance // information for the instances
	majority	int
	done		[]int
}

// if instance is not init'd, initialise
func (px *Paxos) ensureInstanceInit(seq int) {
	_, ok := px.instances[seq]
	if !ok {
		px.instances[seq] = initInstance()
	}
}

func (px *Paxos) setDecision(seq int, decidedVal interface{}) {
	px.ensureInstanceInit(seq)
	px.instances[seq].fate = Decided
	px.instances[seq].decidedVal = decidedVal
}

// advance the next proposal number by incrementing n_p's num component.
// n_p's me component stays the same (should be this peer's me)
func (px *Paxos) nextPNum(seq int) PNum {
	instance, ok := px.instances[seq]
	if !ok {
		log.Fatal("tried to advance proposal number but instance",
				  "information was not found for sequence number", seq)
		return PNum{Num: -1, Me: -1}
	}
	var new_num = instance.n_p.Num + 1
	return PNum{Num: new_num, Me: px.me}
}

// returns whether seq has been decided and its value
func (px *Paxos) decided(seq int) (bool, interface{}) {
	instance, ok := px.instances[seq]
	if ok && instance.fate == Decided {
		return true, instance.decidedVal
	}
	return false, nil
}

//
// the application wants paxos to start agreement on
// instance seq, with proposed value v.
// Start() returns right away; the application will
// call Status() to find out if/when agreement
// is reached.
//
func (px *Paxos) Start(seq int, v interface{}) {
	/*
	while not decided:
      choose n, unique and higher than any n seen so far
      send prepare(n) to all servers including self
      if prepare_ok(n, n_a, v_a) from majority:
        v' = v_a with highest n_a; choose own v otherwise
        send accept(n, v') to all
        if accept_ok(n) from majority:
          send decided(v') to all
	*/
	go func() {
		for {
			if px.isdead() {
				break
			}
			
			// Break loop if already decided on val
			px.mu.Lock()
			dec, _ := px.decided(seq)
			px.mu.Unlock()
			if dec {
				return
			}
			
			debug("Start! seq:", seq, ", v:", v)
			
			// If no information stored on seq, initialise
			px.mu.Lock()
			px.ensureInstanceInit(seq)
			// Find next proposal number
			next_n_p := px.nextPNum(seq)

			// args don't need to be reinitialised per prepare request
			var args = &PrepareArgs{}
			args.Me = px.me // callee will use this to identify itself as local or remote host
			args.Seq = seq
			args.N = next_n_p
			
			px.mu.Unlock()

			var numAccepted int = 0 // needs to reach px.majority to proceed
			var v_ interface{} = v
			var highest_n_a PNum = PNum{Num: -1, Me: -1}

			// send prepare requests to each peer
			for idx, host := range px.peers {
			    var reply PrepareReply
				if px.me == idx {
					// Run the local acceptor prepare
					px.Prepare(args, &reply)
				} else {
					// call prepare rpc
					success := call(host, "Paxos.Prepare", args, &reply)
					if !success {
						continue
					}
				}
				
				// Handle any Done value piggybacking the reply
				px.setDone(idx, reply.Done)
				
				// check to see if it has been decided already
				if reply.Decided {
					px.sendDecision(seq, reply.DecidedVal)
					return
				}
				
				if reply.Ok {
					numAccepted++
					if comparePNum(reply.N_a, highest_n_a) > 0 {
						v_ = reply.V_a
						highest_n_a = reply.N_a
					}
				}
			}
			
			if (numAccepted < px.majority) {
				continue
			}
			
			// reset
			numAccepted = 0
			
			acceptArgs := &AcceptArgs{}
			acceptArgs.Me = px.me
			acceptArgs.Seq = seq
			acceptArgs.N = next_n_p
			acceptArgs.V_ = v_
			
			for idx, host := range px.peers {
				var acceptReply AcceptReply = AcceptReply{}
				if px.me == idx {
					// Run the local acceptor prepare
					px.Accept(acceptArgs, &acceptReply)
				} else {
					success := call(host, "Paxos.Accept", acceptArgs, &acceptReply)
					if !success {
						continue
					}
				}
				
				// Handle any Done value piggybacking the reply
				px.setDone(idx, acceptReply.Done)
				
				if acceptReply.Ok {
					debug("Accept OK by:", host)
					numAccepted++
				}
				
			}
			
			if numAccepted >= px.majority {
				debug("Majority decision made! V_:", acceptArgs.V_)
				px.sendDecision(seq, acceptArgs.V_)
				return
			}
			
		}
	}();
}

func (px *Paxos) sendDecision(seq int, decidedVal interface{}) {
	px.mu.Lock()
	px.setDecision(seq, decidedVal)
	px.mu.Unlock()
				
	decideArgs := &DecideArgs{}
	decideArgs.Seq = seq
	decideArgs.V = decidedVal
				
	for idx, host := range px.peers {
		var reply DecideReply = DecideReply{}
		if px.me != idx {
		   call(host, "Paxos.Decide", decideArgs, &reply)
		}
	}
}

//
// the application on this machine is done with
// all instances <= seq.
//
// see the comments for Min() for more explanation.
//
func (px *Paxos) Done(seq int) {
	px.mu.Lock()
	defer px.mu.Unlock()
	
	if (px.done[px.me] < seq) {
		px.done[px.me] = seq
	}
}

func (px *Paxos) setDone(peer int, seq int) {
	px.mu.Lock()
	if px.done[peer] < seq {
		px.done[peer] = seq
	}
	px.mu.Unlock()
	
	// will remove from memory instances entries no longer needed
	px.Min()	
}

//
// the application wants to know the
// highest instance sequence known to
// this peer.
//
func (px *Paxos) Max() int {
	px.mu.Lock()
	defer px.mu.Unlock()
	
	var s_ int = -1
	for seq, _ := range px.instances {
		if seq > s_ { s_ = seq }
	}
	return s_
}

//
// Min() should return one more than the minimum among z_i,
// where z_i is the highest number ever passed
// to Done() on peer i. A peers z_i is -1 if it has
// never called Done().
//
// Paxos is required to have forgotten all information
// about any instances it knows that are < Min().
// The point is to free up memory in long-running
// Paxos-based servers.
//
// Paxos peers need to exchange their highest Done()
// arguments in order to implement Min(). These
// exchanges can be piggybacked on ordinary Paxos
// agreement protocol messages, so it is OK if one
// peers Min does not reflect another Peers Done()
// until after the next instance is agreed to.
//
// The fact that Min() is defined as a minimum over
// *all* Paxos peers means that Min() cannot increase until
// all peers have been heard from. So if a peer is dead
// or unreachable, other peers Min()s will not increase
// even if all reachable peers call Done. The reason for
// this is that when the unreachable peer comes back to
// life, it will need to catch up on instances that it
// missed -- the other peers therefor cannot forget these
// instances.
//
func (px *Paxos) Min() int {
	px.mu.Lock()
	defer px.mu.Unlock()
	var min int = px.done[0]
	for _, d := range px.done {
		if min > d { min = d }
	}

	min += 1
	// remove entries no longer needed
	for idx, _ := range px.instances {
		if idx < min {
			delete(px.instances, idx)
		}
	}
	return min
}

//
// the application wants to know whether this
// peer thinks an instance has been decided,
// and if so what the agreed value is. Status()
// should just inspect the local peer state;
// it should not contact other Paxos peers.
//
func (px *Paxos) Status(seq int) (Fate, interface{}) {
	if seq < px.Min() {
		return Forgotten, nil
	}
	
	px.mu.Lock()
	defer px.mu.Unlock()
	
	inst, ok := px.instances[seq]
	if ok {
		return inst.fate, inst.decidedVal		
	}
	return Pending, nil
}



//
// tell the peer to shut itself down.
// for testing.
// please do not change these two functions.
//
func (px *Paxos) Kill() {
	atomic.StoreInt32(&px.dead, 1)
	if px.l != nil {
		px.l.Close()
	}
}

//
// has this peer been asked to shut down?
//
func (px *Paxos) isdead() bool {
	return atomic.LoadInt32(&px.dead) != 0
}

// please do not change these two functions.
func (px *Paxos) setunreliable(what bool) {
	if what {
		atomic.StoreInt32(&px.unreliable, 1)
	} else {
		atomic.StoreInt32(&px.unreliable, 0)
	}
}

func (px *Paxos) isunreliable() bool {
	return atomic.LoadInt32(&px.unreliable) != 0
}

//
// the application wants to create a paxos peer.
// the ports of all the paxos peers (including this one)
// are in peers[]. this servers port is peers[me].
//
func Make(peers []string, me int, rpcs *rpc.Server) *Paxos {
	px := &Paxos{}
	px.me = me
	px.peers = peers
	px.majority = (len(px.peers) / 2) + 1
	px.done = make([]int, len(px.peers))
	for idx, _ := range px.done {
		px.done[idx] = -1
	}
	px.instances = make(map[int]*Instance)

	if rpcs != nil {
		// caller will create socket &c
		rpcs.Register(px)
	} else {
		rpcs = rpc.NewServer()
		rpcs.Register(px)

		// prepare to receive connections from clients.
		// change "unix" to "tcp" to use over a network.
		os.Remove(peers[me]) // only needed for "unix"
		l, e := net.Listen("unix", peers[me])
		if e != nil {
			log.Fatal("listen error: ", e)
		}
		px.l = l

		// please do not change any of the following code,
		// or do anything to subvert it.

		// create a thread to accept RPC connections
		go func() {
			for px.isdead() == false {
				conn, err := px.l.Accept()
				if err == nil && px.isdead() == false {
					if px.isunreliable() && (rand.Int63()%1000) < 100 {
						// discard the request.
						conn.Close()
					} else if px.isunreliable() && (rand.Int63()%1000) < 200 {
						// process the request but force discard of reply.
						c1 := conn.(*net.UnixConn)
						f, _ := c1.File()
						err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
						if err != nil {
							fmt.Printf("shutdown: %v\n", err)
						}
						atomic.AddInt32(&px.rpcCount, 1)
						go rpcs.ServeConn(conn)
					} else {
						atomic.AddInt32(&px.rpcCount, 1)
						go rpcs.ServeConn(conn)
					}
				} else if err == nil {
					conn.Close()
				}
				if err != nil && px.isdead() == false {
					fmt.Printf("Paxos(%v) accept: %v\n", me, err.Error())
				}
			}
		}()
	}


	return px
}


//
// call() sends an RPC to the rpcname handler on server srv
// with arguments args, waits for the reply, and leaves the
// reply in reply. the reply argument should be a pointer
// to a reply structure.
//
// the return value is true if the server responded, and false
// if call() was not able to contact the server. in particular,
// the replys contents are only valid if call() returned true.
//
// you should assume that call() will time out and return an
// error after a while if it does not get a reply from the server.
//
// please use call() to send all RPCs, in client.go and server.go.
// please do not change this function.
//
func call(srv string, name string, args interface{}, reply interface{}) bool {
	c, err := rpc.Dial("unix", srv)
	if err != nil {
		err1 := err.(*net.OpError)
		if err1.Err != syscall.ENOENT && err1.Err != syscall.ECONNREFUSED {
			fmt.Printf("paxos Dial() failed: %v\n", err1)
		}
		return false
	}
	defer c.Close()

	err = c.Call(name, args, reply)
	if err == nil {
		return true
	}

	fmt.Println(err)
	return false
}

/*

   - Pseudocode -

proposer(v):
  while not decided:
    choose n, unique and higher than any n seen so far
    send prepare(n) to all servers including self
    if prepare_ok(n, n_a, v_a) from majority:
      v' = v_a with highest n_a; choose own v otherwise
      send accept(n, v') to all
      if accept_ok(n) from majority:
        send decided(v') to all

acceptor's state:
  n_p (highest prepare seen)
  n_a, v_a (highest accept seen)

acceptor's prepare(n) handler:
  if n > n_p
    n_p = n
    reply prepare_ok(n, n_a, v_a)
  else
    reply prepare_reject

acceptor's accept(n, v) handler:
  if n >= n_p
    n_p = n
    n_a = n
    v_a = v
    reply accept_ok(n)
  else
    reply accept_reject
	
*/

///
/// RPC
///

type PrepareArgs struct {
	Me			int // identifies the host
	Seq			int
	N			PNum
}

type PrepareReply struct {
	Ok			bool
	N			PNum
	N_a			PNum
	V_a			interface{}
	Decided		bool
	DecidedVal  interface{}
	Done		int
}

type AcceptArgs struct {
	Me			int
	Seq			int
	N			PNum
	V_			interface{} // v' = v_a with highest n_a; choose own v otherwise
}

type AcceptReply struct {
	Ok			bool
	N			PNum // replied ok with the following request num
	Done		int
}

type DecideArgs struct {
	// we've decided that instance corresponding to
	// the sequence number will have the value
	Seq		int
	V		interface{}
}

type DecideReply struct {
	Ok		bool // set to true if acknowledge the decision
	Done	int
}

func (px *Paxos) Prepare(args *PrepareArgs, reply *PrepareReply) error {
	px.mu.Lock()
	defer px.mu.Unlock()

    // grab the peer's latest (local) info for the sequence number.
	// may need to initialise info state.
	inst, ok := px.instances[args.Seq]
	if !ok {
		px.instances[args.Seq] = initInstance()
		inst = px.instances[args.Seq]
	}
	
	reply.Done = px.done[px.me]
	
	// if decided already, return decided val
	if inst.fate == Decided {
		reply.Decided = true
		reply.DecidedVal = inst.decidedVal
		return nil
	}
	
	// if n > n_p
	if comparePNum(args.N, inst.n_p) > 0 {
		inst.n_p = args.N
		reply.Ok = true
		reply.N = args.N
		reply.N_a = inst.n_a
		reply.V_a = inst.v_a
		debug("prepare_ok, n =", args.N)
		return nil
	} else {
		debug("prepare_reject, n_p =", inst.n_p)
	}
	
	reply.Ok = false
	return nil
}

func (px *Paxos) Accept(args *AcceptArgs, reply *AcceptReply) error {
	px.mu.Lock()
	defer px.mu.Unlock()
	
	px.ensureInstanceInit(args.Seq)
	inst, _ := px.instances[args.Seq]
	
	reply.Done = px.done[px.me]
	
	if comparePNum(args.N, inst.n_p) < 0 {
		reply.Ok = false
		return nil
	}
	
	inst.n_p = args.N
	inst.n_a = args.N
	inst.v_a = args.V_
	reply.Ok = true
	reply.N = args.N
	return nil
}

func (px *Paxos) Decide(args *DecideArgs, reply *DecideReply) error {
	px.mu.Lock()
	defer px.mu.Unlock()
	
	reply.Done = px.done[px.me]
	
	px.setDecision(args.Seq, args.V)
	reply.Ok = true
	return nil
}


