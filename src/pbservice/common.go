package pbservice

const (
	OK             = "OK"
	ErrNoKey       = "ErrNoKey"
	ErrWrongServer = "ErrWrongServer"
	PutOp          = "Put"
	AppendOp       = "Append"
)

type Err string

// Put or Append
type PutAppendArgs struct {
	Key			string
	Value		string
	Op			string
	ClientId	int64
	Xid			uint
	Forward		bool

	// Field names must start with capital letters,
	// otherwise RPC will break.
}

type PutAppendReply struct {
	Err Err
	NewVal string
}

type GetArgs struct {
	Key			string
	ClientId	int64
	Xid			uint
	Forward		bool
}

type GetReply struct {
	Err   Err
	Value string
}

type StateTransferArgs struct {
	Backup		string

}

type StateTransferReply struct {
	State map[string]string
	GetCache	map[int64]map[uint]GetReply
	PutCache	map[int64]map[uint]PutAppendReply
}
