package pbservice

import "net"
import "fmt"
import "net/rpc"
import "log"
import "time"
import "viewservice"
import "sync"
import "sync/atomic"
import "os"
import "syscall"
import "math/rand"



type PBServer struct {
	mu         sync.Mutex
	l          net.Listener
	dead       int32 // for testing
	unreliable int32 // for testing
	me         string
	vs         *viewservice.Clerk
	view		viewservice.View
	initialised	bool
	state		map[string]string
	getCache	map[int64]map[uint]GetReply
	putCache	map[int64]map[uint]PutAppendReply
}


func (pb *PBServer) Get(args *GetArgs, reply *GetReply) error {

	pb.mu.Lock()
	
	if pb.me != pb.view.Primary {
		if !(pb.me == pb.view.Backup && args.Forward) {
			// not primary, and not a forwarded request to backup
			reply.Err = ErrWrongServer
			pb.mu.Unlock()
			return fmt.Errorf("GET: " + pb.me + " is not primary and not a forwarded backup")
		}
		
		if (pb.me == pb.view.Backup && !pb.initialised) {
			// uninitialised backup can't handle requests yet
			reply.Err = ErrWrongServer
			pb.mu.Unlock()
			return fmt.Errorf("GET: Uninitialised backup %s", pb.me)
		}
	} else if args.Forward {
		// primary rejects forwarded requests
		reply.Err = ErrWrongServer
		pb.mu.Unlock()
		return fmt.Errorf("GET: Primary " + pb.me + " rejects forwarded requests")
	}

	_, ok := pb.getCache[args.ClientId]
	if !ok {
		pb.getCache[args.ClientId] = make(map[uint]GetReply)
	}
	cachedReply, ok := pb.getCache[args.ClientId][args.Xid]
	if ok {
		*reply = cachedReply
		if cachedReply.Err != OK {
			pb.mu.Unlock()
			return fmt.Errorf("Cached response held an error")
		}
		pb.mu.Unlock()
		return nil
	}

	// Forward to backup IFF backup exists and this is not a forwarded
	// request itself. If the call fails, return an error.
	// The client will then try to fetch the latest view and retry.
	if pb.me == pb.view.Primary && pb.view.Backup != "" {
		var fwdArgs *GetArgs = &GetArgs{}
		*fwdArgs = *args
		fwdArgs.Forward = true
		var fwdReply GetReply
		pb.mu.Unlock()
		ok := call(pb.view.Backup, "PBServer.Get", fwdArgs, &fwdReply)
		pb.mu.Lock()
		if !ok {
			reply.Err = ErrWrongServer
			pb.mu.Unlock()
			return fmt.Errorf("GET: Failed to forward to backup " + pb.view.Backup)
		} else {
			// Force using same reply as backup
			*reply = fwdReply
			pb.getCache[args.ClientId][args.Xid] = fwdReply
			pb.mu.Unlock()
			return nil
		}
	} else {
		// Nothing to do
		//
		// if pb.view.Backup == "", then primary will continue going on
	}
	
	// if it comes to this, this means we're either the primary that doesn't
	// think it has a backup, or the backup handling a forwarded request
	
	val, ok := pb.state[args.Key]
	if !ok {
		reply.Err = ErrNoKey
		reply.Value = ""
	} else {
		reply.Err = OK
		reply.Value = val
	}
	pb.getCache[args.ClientId][args.Xid] = *reply
	pb.mu.Unlock()
	return nil
}


func (pb *PBServer) PutAppend(args *PutAppendArgs, reply *PutAppendReply) error {

	pb.mu.Lock()
	
	if pb.me != pb.view.Primary {
		if !(pb.me == pb.view.Backup && args.Forward) {
			// not primary, and not a forwarded request to backup
			reply.Err = ErrWrongServer
			pb.mu.Unlock()
			return fmt.Errorf("PutAppend: Wrong server %s", pb.me)
		}
		
		if (pb.me == pb.view.Backup && !pb.initialised) {
			// uninitialised backup can't handle requests yet
			reply.Err = ErrWrongServer
			pb.mu.Unlock()
			return fmt.Errorf("PUT: Uninitialised backup %s", pb.me)
		}
	} else if args.Forward {
		// as primary, reject forwarded requests
		reply.Err = ErrWrongServer
		pb.mu.Unlock()
		return fmt.Errorf("GET: Wrong server %s", pb.me)
	}
	
	_, ok := pb.putCache[args.ClientId]
	if !ok {
		pb.putCache[args.ClientId] = make(map[uint]PutAppendReply)
	}
	cachedReply, ok := pb.putCache[args.ClientId][args.Xid]
	if ok {
		*reply = cachedReply
		if cachedReply.Err != OK {
			pb.mu.Unlock()
			return fmt.Errorf("Cached response held an error")
		}
		pb.mu.Unlock()
		return nil
	}

	// Forward to backup IFF backup exists and this is not a forwarded
	// request itself. If the call fails, return an error.
	// The client will then try to fetch the latest view and retry.
	if pb.me == pb.view.Primary && pb.view.Backup != "" {
		var fwdArgs *PutAppendArgs = &PutAppendArgs{}
		*fwdArgs = *args
		fwdArgs.Forward = true
		var fwdReply PutAppendReply
		pb.mu.Unlock()
		ok := call(pb.view.Backup, "PBServer.PutAppend", fwdArgs, &fwdReply)
		pb.mu.Lock()
		if !ok {
			reply.Err = ErrWrongServer
			pb.mu.Unlock()
			return fmt.Errorf("PutAppend: Failed to forward to backup %s", pb.view.Backup)
		} else {
			// Force using same reply as backup
			/*
			pb.putCache[args.ClientId][args.Xid] = fwdReply
			pb.state[args.Key] = fwdReply.NewVal
			*reply = fwdReply
			pb.mu.Unlock()
			return nil
			*/
		}
	}
	
	newVal := args.Value
	val, ok := pb.state[args.Key]
	if args.Op == AppendOp && ok {
		newVal = val + newVal
	}
	
	pb.state[args.Key] = newVal
	reply.Err = OK
	reply.NewVal = newVal
	pb.putCache[args.ClientId][args.Xid] = *reply
	pb.mu.Unlock()
	return nil
}

func (pb *PBServer) StateTransfer(args *StateTransferArgs,
                                  reply *StateTransferReply) error {
	pb.mu.Lock()
	
	if pb.me != pb.view.Primary {
		pb.mu.Unlock()
		return fmt.Errorf("PBServer " + pb.me + " not primary so cannot transfer state")
	}
	
	if args.Backup != pb.view.Backup {
		pb.mu.Unlock()
		return fmt.Errorf("Primary " + pb.me + " does not recognize " + args.Backup +
							" as backup yet so no state transfer. " +
							"Primary thinks backup is " + pb.view.Backup);
	}
	
	reply.State = pb.state
	reply.GetCache = pb.getCache
	reply.PutCache = pb.putCache
	//fmt.Println(reply.GetCache)
	//fmt.Println(reply.PutCache)
	pb.mu.Unlock()
	return nil
}


//
// ping the viewserver periodically.
// if view changed:
//   transition to new view.
//   manage transfer of state from primary to new backup.
//
func (pb *PBServer) tick() {
	pb.mu.Lock()

	// Ping the viewserver and get the view
	view, err := pb.vs.Ping(pb.view.Viewnum)
	if err != nil {
		fmt.Println("PBServer " + pb.me + ": error when pinging viewservice")
		pb.mu.Unlock()
		return
	}
	
	if pb.me == view.Primary && view.Viewnum == 1 && !pb.initialised {
		pb.initialised = true
		fmt.Println("Primary " + pb.me + " initialised")
		
	} else if pb.me == view.Backup && !pb.initialised {
		// If this server is an uninitialised backup, then we need
		// to get the state from primary.
		args := &StateTransferArgs{}
		// the primary will only state transfer if it recognizes this backup
		// in its view
		args.Backup = pb.me 
		var reply StateTransferReply
		pb.mu.Unlock()
		ok := call(view.Primary, "PBServer.StateTransfer", args, &reply)
		pb.mu.Lock()
		if ok {
			pb.state = reply.State
			pb.getCache = reply.GetCache
			pb.putCache = reply.PutCache
			pb.initialised = true
			fmt.Println("Backup " + pb.me + " initialised")
		} else {
			// backup left uninitialised
			// won't be able to respond to get/put requests
			// wait till next tick to try again

		}
	}
	
	if (pb.me == pb.view.Backup && pb.me == view.Primary) {
		fmt.Println("Promoted from backup to primary", pb.me)
	}
	
	pb.view = view
	pb.mu.Unlock()
}

// tell the server to shut itself down.
// please do not change these two functions.
func (pb *PBServer) kill() {
	atomic.StoreInt32(&pb.dead, 1)
	pb.l.Close()
}

// call this to find out if the server is dead.
func (pb *PBServer) isdead() bool {
	return atomic.LoadInt32(&pb.dead) != 0
}

// please do not change these two functions.
func (pb *PBServer) setunreliable(what bool) {
	if what {
		atomic.StoreInt32(&pb.unreliable, 1)
	} else {
		atomic.StoreInt32(&pb.unreliable, 0)
	}
}

func (pb *PBServer) isunreliable() bool {
	return atomic.LoadInt32(&pb.unreliable) != 0
}


func StartServer(vshost string, me string) *PBServer {
	pb := new(PBServer)
	pb.me = me
	pb.vs = viewservice.MakeClerk(me, vshost)
	// Your pb.* initializations here.
	pb.view.Viewnum = 0
	pb.initialised = false
	pb.state = make(map[string]string)
	pb.getCache = make(map[int64]map[uint]GetReply)
	pb.putCache = make(map[int64]map[uint]PutAppendReply)

	rpcs := rpc.NewServer()
	rpcs.Register(pb)

	os.Remove(pb.me)
	l, e := net.Listen("unix", pb.me)
	if e != nil {
		log.Fatal("listen error: ", e)
	}
	pb.l = l

	// please do not change any of the following code,
	// or do anything to subvert it.

	go func() {
		for pb.isdead() == false {
			conn, err := pb.l.Accept()
			if err == nil && pb.isdead() == false {
				if pb.isunreliable() && (rand.Int63()%1000) < 100 {
					// discard the request.
					conn.Close()
				} else if pb.isunreliable() && (rand.Int63()%1000) < 200 {
					// process the request but force discard of reply.
					c1 := conn.(*net.UnixConn)
					f, _ := c1.File()
					err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
					if err != nil {
						fmt.Printf("shutdown: %v\n", err)
					}
					go rpcs.ServeConn(conn)
				} else {
					go rpcs.ServeConn(conn)
				}
			} else if err == nil {
				conn.Close()
			}
			if err != nil && pb.isdead() == false {
				fmt.Printf("PBServer(%v) accept: %v\n", me, err.Error())
				pb.kill()
			}
		}
	}()

	go func() {
		for pb.isdead() == false {
			pb.tick()
			time.Sleep(viewservice.PingInterval)
		}
	}()

	return pb
}
