package shardkv

//
// Sharded key/value server.
// Lots of replica groups, each running op-at-a-time paxos.
// Shardmaster decides which group serves each shard.
// Shardmaster may change shard assignment from time to time.
//
// You will have to modify these definitions.
//

const (
	OK            = "OK"
	ErrNoKey      = "ErrNoKey"
	ErrWrongGroup = "ErrWrongGroup"
    NotAvailable = "NotAvailable"
    PutOp = "Put"
    AppendOp = "Append"
    GetOp = "Get"
    ReconfigOp = "Reconfig"
)

type Err string

type PutAppendArgs struct {
	Key   string
	Value string
	Op    string // "Put" or "Append"
    ClientXid Xid
}

type PutAppendReply struct {
	Err Err
}

type GetArgs struct {
	Key string
    Op string // "Get"
    ClientXid Xid
}

type GetReply struct {
	Err   Err
	Value string
}

type GrabStateArgs struct {
    ConfigNum int
}

type GrabStateReply struct {
    SavedState map[string]string
    SavedXidCache map[int]map[Xid]string
    Err Err
}