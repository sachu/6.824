package shardkv

import "net"
import "fmt"
import "net/rpc"
import "log"
import "time"
import "paxos"
import "sync"
import "sync/atomic"
import "os"
import "syscall"
import "encoding/gob"
import "math/rand"
import "shardmaster"


const Debug = 0

func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		log.Printf(format, a...)
	}
	return
}


type Op struct {
    Operation string // get, put, append, reconfig
    ClientXid Xid
    Key string
    Value string
    ConfigNum int
}


type ShardKV struct {
	mu         sync.Mutex
	l          net.Listener
	me         int
	dead       int32 // for testing
	unreliable int32 // for testing
	sm         *shardmaster.Clerk
	px         *paxos.Paxos
    servers []string
    name string
    /*
    We update state sequentially. seq is the next instance update
    to this server's local state. state reflects state up to instance number (seq-1).
    */
	seq int
    state map[string]string // stores the KV pairs
    xidCache map[int]map[Xid]string // [shardNum -> [Xid -> value]]

    // [config# -> KV pairs that this group has to hand
    //             off to other groups in the next config]
    savedState map[int]map[string]string
    // [config# -> client state that needs to be passed to others]
    savedXidCache map[int]map[int]map[Xid]string

	gid int64 // my replica group ID
    localConfig shardmaster.Config
}

func (kv *ShardKV) isResponsible(key string) bool {
    responsibleGid := kv.localConfig.Shards[key2shard(key)]
    return kv.gid == responsibleGid
}

func (kv *ShardKV) Get(args *GetArgs, reply *GetReply) error {
    kv.mu.Lock()
    defer kv.mu.Unlock()
    if !kv.isResponsible(args.Key) {
        DPrintf("Gid=%d not responsible for key=%s", kv.gid, args.Key)
        reply.Err = ErrWrongGroup
        return nil
    } else {
        DPrintf("Gid=%d IS responsible for key=%s", kv.gid, args.Key)
    }

    // Check to see if there is already a cached response for this XID
    shardNum := key2shard(args.Key)
    getValue, ok := kv.xidCache[shardNum][args.ClientXid]
    if ok {
        DPrintf("Have already processed XID=%s", args.ClientXid)
        reply.Value = getValue
        reply.Err = OK
        return nil
    }

    var attemptedOp Op = Op{}
	attemptedOp.Operation = args.Op
    attemptedOp.ClientXid = args.ClientXid
    attemptedOp.Key = args.Key

    committedInstanceNum := kv.logOp(attemptedOp)
    if (committedInstanceNum < 0) {
        // While trying to commit the operation to Paxos, a reconfiguration
        // occurred rendering this group not responsible for the Key. So
        // we will tell the client ErrWrongGroup and the client will retry.
        reply.Err = ErrWrongGroup
        return nil
    }
    DPrintf("Get op %s has instance num %d", attemptedOp, committedInstanceNum)
    kv.updateStateToInstanceNum(committedInstanceNum)
    reply.Value, _ = kv.xidCache[shardNum][args.ClientXid]
    reply.Err = OK
    return nil
}

// RPC handler for client Put and Append requests
func (kv *ShardKV) PutAppend(args *PutAppendArgs, reply *PutAppendReply) error {
    kv.mu.Lock()
    defer kv.mu.Unlock()
    if !kv.isResponsible(args.Key) {
        DPrintf("Gid=%d not responsible for key=%s", kv.gid, args.Key)
        reply.Err = ErrWrongGroup
        return nil
    } else {
        DPrintf("Gid=%d IS responsible for key=%s", kv.gid, args.Key)
    }

    // Check to see if there is already a cached response for this XID
    shardNum := key2shard(args.Key)
    _, ok := kv.xidCache[shardNum][args.ClientXid]
    if ok {
        DPrintf("Have already processed XID=%s", args.ClientXid)
        reply.Err = OK
        return nil
    }

    var attemptedOp Op = Op{}
    attemptedOp.Operation = args.Op
    attemptedOp.ClientXid = args.ClientXid
    attemptedOp.Key = args.Key
    attemptedOp.Value = args.Value

    committedInstanceNum := kv.logOp(attemptedOp)
    if (committedInstanceNum < 0) {
        // While trying to commit the operation to Paxos, a reconfiguration
        // occurred rendering this group not responsible for the Key. So
        // we will tell the client ErrWrongGroup and the client will retry.
        reply.Err = ErrWrongGroup
        return nil
    }
    DPrintf("PutAppend op %s has instance num %d", attemptedOp, committedInstanceNum)
    kv.updateStateToInstanceNum(committedInstanceNum)
    reply.Err = OK
    return nil
}

// Ask the shardmaster if there's a new configuration.
// If so, re-configure.
func (kv *ShardKV) tick() {
    kv.mu.Lock()
    defer kv.mu.Unlock()
    smConfig := kv.sm.Query(-1) // fetch latest config from Shardmaster.
    DPrintf("TICK\nLocal current config: %s\nShardmaster current config: %s",
            kv.localConfig, smConfig)
    if (kv.localConfig.Num == smConfig.Num) {
        DPrintf("Local config num equal to shardmaster config num. Nothing to do.")
        return
    } else {
        DPrintf("Local config is behind shardmaster config. Now processing reconfigurations.")
    }

    for nextConfigNum := kv.localConfig.Num + 1 ;
        nextConfigNum <= smConfig.Num;
        nextConfigNum++ {
        kv.reconfigure(nextConfigNum)
    }
}

// Log an operation in Paxos (retry until successful).
// Return the instance number of the logged operation.
func (kv *ShardKV) logOp(attemptedOp Op) int {
    seqToDecide := kv.seq
    for {
        // TODO
        // The following logic of setting seqToDecide to paxos.Max() + 1
        // fails in the concurrent, unreliable test.
        // We keep it here as a note.
        // seqToDecide := kv.px.Max() + 1 // Next known instance number to decide.

        // The following for loop is only useful for the above obsolete declaration of seqToDecide.
        // Now that we start with setting seqToDecide to just kv.seq, the for
        // loop will be a no-op.
        //
        // Before every call to Paxos.Start(seqToDecide, op), we scan the
        // instances numbered kv.seq (next seq to update local state) to seqToDecide - 1.
        // If we find that the op has already been logged, we return that instance number.
        // This is necessary because another server in the same group could have
        // logged the same operation.
        for decidedSeq := kv.seq ; decidedSeq < seqToDecide ; decidedSeq++ {
            fate, alreadyLoggedOp := kv.px.Status(decidedSeq)
            // !!!!!
            // We need to make sure when we scan for repeats to wait until fate has been Decided.
            // Else we can run into a situation where sequence 46 was not yet Decided but is the
            // same operation as seqToDecide but logged by another peer. If we don't wait until
            // the decision, we'll skip over a possible duplicate.
            for fate != paxos.Decided {
                DPrintf("Waiting for fate to be Decided. Seq=%d, fate=%s", decidedSeq, fate)
                time.Sleep(time.Millisecond * 100)
                fate, alreadyLoggedOp = kv.px.Status(decidedSeq)
            }
            if attemptedOp == alreadyLoggedOp {
                DPrintf("Repeat attempt: Already logged the op %s at instance num %d", attemptedOp, decidedSeq)
                return decidedSeq
            }
        }

        // We fast forward our local state to [seqToDecide - 1].
        kv.updateStateToInstanceNum(seqToDecide - 1)
        // Now kv.seq == seqToDecide - 1
        if attemptedOp.Operation != ReconfigOp && !kv.isResponsible(attemptedOp.Key) {
            // If the operation is a Get or Put/Append and we are no longer
            // responsible for the Key because of a reconfiguration that happened
            // once we updated our state, we will return -1 to signal this group
            // is not responsible for the key.
            DPrintf("ALERT: Reconfiguration happened before this op(%s), and the group(%d) is no longer responsible",
                    attemptedOp, kv.gid)
            return -1
        }

        // Go ahead and start the negotiation and wait until seqToDecide has been decided.
        kv.px.Start(seqToDecide, attemptedOp)
        to := 10 * time.Millisecond
        for {
            status, decidedOp := kv.px.Status(seqToDecide)
            DPrintf("me: %d, seqToDecide: %d, attempted op: %s\n   status: %s, decided op: %s",
                    kv.me, seqToDecide, attemptedOp, status, decidedOp)
            if status == paxos.Pending {
                // Do nothing because nothing has been decided. Will sleep again.
            } else if status == paxos.Forgotten {
                // Need to fast forward. This situation can happen
                // if this thread was interrupted and another thread
                // had processed agreements. Then when we context switch
                // back to this thread, the status might have been forgotten
                // because the other thread had called Done.
                //
                // This should not happen because we are locking on every request and tick.
                log.Fatalln("Tried to find status of a forgotten instance")
                break
            } else if status == paxos.Decided {
                decidedOp := decidedOp.(Op)
                if attemptedOp == decidedOp {
                    DPrintf("Success: Attempted op has been logged into paxos! Instance num=%d", seqToDecide)
                    return seqToDecide
                } else {
                    // The requested op was not committed, so we will reset seqToDecide and re-Start().
                    seqToDecide++
                    break
                }
            }
            // seqToDecide not decided yet, so sleep.
            time.Sleep(to)
            if to < 10 * time.Second {
                to *= 2
            }
        }
        time.Sleep(time.Millisecond * 200)
    }
}


func (kv *ShardKV) reconfigure(configNum int) {
    // We log the reconfiguration in Paxos. Then we update local state to
    // the instance number of the reconfiguration.
    var attemptedOp Op = Op{}
	attemptedOp.Operation = ReconfigOp
    attemptedOp.ConfigNum = configNum
    reconfigInstanceNum := kv.logOp(attemptedOp)
    DPrintf("Reconfig %d has instance num %d, NAME=%s", configNum, reconfigInstanceNum, kv.name)
    kv.updateStateToInstanceNum(reconfigInstanceNum)
}

func (kv *ShardKV) updateStateToInstanceNum(instanceNum int) {
    for seq := kv.seq ; seq <= instanceNum ; seq++ {
        DPrintf("NAME: %s, Updating Sequence: %d", kv.name, seq)
        fate, val := kv.px.Status(seq)
        for fate != paxos.Decided {
            DPrintf("Waiting for fate to be Decided. Seq=%d, fate=%s", seq, fate)
            time.Sleep(time.Millisecond * 100)
            fate, val = kv.px.Status(seq)
        }
        op := val.(Op)
        if op.Operation == ReconfigOp {
            kv.handleReconfig(op.ConfigNum)
        } else if op.Operation == PutOp || op.Operation == AppendOp {
            kv.handlePutAppend(op)
        } else if op.Operation == GetOp {
            kv.handleGet(op)
        } else {
            log.Fatalln("FATAL: Unrecognized operation", op)
        }
        kv.px.Done(seq)
        kv.seq = seq + 1
    }
}

func (kv *ShardKV) handlePutAppend(op Op) {
    newVal := op.Value
    if op.Operation == AppendOp {
        existingVal, ok := kv.state[op.Key]
        if ok {
            newVal = existingVal + op.Value
        }
    }
    kv.state[op.Key] = newVal
    kv.xidCache[key2shard(op.Key)][op.ClientXid] = "PutAppendComplete"
    DPrintf("Finished handling put/append: key=%s, val=%s", op.Key, kv.state[op.Key])
}

func (kv *ShardKV) handleGet(op Op) {
    val, ok := kv.state[op.Key]
    if ok {
        kv.xidCache[key2shard(op.Key)][op.ClientXid] = val
    } else {
        kv.xidCache[key2shard(op.Key)][op.ClientXid] = ""
    }
}

func (kv *ShardKV) handleReconfig(configNum int) {
    oldConfig := kv.localConfig
    nextConfig := kv.sm.Query(configNum)
    DPrintf("Reconfiguring from config %d to %d", oldConfig.Num, nextConfig.Num)
    if nextConfig.Num - oldConfig.Num != 1 {
        log.Fatalf("FATAL: New config %d is not 1 more than old config %d. We should process reconfigurations one by one. NAME=%s",
          nextConfig.Num, oldConfig.Num, kv.name)
    }

    // (1) For every shard this server needs to hand off to another group,
    // store the shard state and its accompanying client state. The newly
    // responsible group will eventually (or perhaps already started)
    // try to grab this state.
    //
    // (2) For every shard this server is newly responsible for,
    // grab the shard state from the formerly responsible group.
    //
    // (3) For all other shards, nothing to do as we will continue
    // processing their requests as always.

    newShards, oldShards := kv.getNewAndOldShards(oldConfig, nextConfig)

    DPrintf("GID: %d, Newly assigned shards: %s\nOld shards to pass on: %s",
           kv.gid, newShards, oldShards)
    
    // (1)
    kv.saveState(oldConfig.Num, oldShards)
    
    // (2)
    kv.retrieveState(oldConfig, newShards)
    
    // (3)
    // Nothing to do.
    
    // Finally, advance the config.
    kv.localConfig = nextConfig
}

func (kv *ShardKV) retrieveState(config shardmaster.Config, newShards []int) {
    for _, newShard := range newShards {
        gidToRetrieve := config.Shards[newShard]
        if gidToRetrieve == 0 {
            // If the group id is 0, that means this shard has no state
            // and we are in the initial stages of transferring to
            // configuration 1. We skip.
            continue
        }

        servers, ok := config.Groups[gidToRetrieve]
        if !ok {
            log.Fatalf("Could not find servers to retrieve shard %d at config %d", newShard, config.Num)
        }

        received := false
        for !received {
            for _, srv := range servers {
                args := &GrabStateArgs{}
                args.ConfigNum = config.Num
                var reply GrabStateReply
                ok := call(srv, "ShardKV.GrabState", args, &reply)
                if ok && reply.Err == OK {
                    for key, value := range reply.SavedState {
                        if key2shard(key) == newShard {
                            kv.state[key] = value
                        }
                    }
                    kv.xidCache[newShard] = reply.SavedXidCache[newShard]
                    received = true
                    break
                }
            }
            time.Sleep(time.Millisecond * 200)
        }
    }
}

func (kv *ShardKV) GrabState(args *GrabStateArgs, reply *GrabStateReply) error {
    DPrintf("GrabStateLock")
    DPrintf("GrabStateUnlock")
    if kv.localConfig.Num <= args.ConfigNum {
        DPrintf("Have not advanced enough to send saved state")
        reply.Err = NotAvailable
        return nil
    }
    reply.SavedState = kv.savedState[args.ConfigNum]
    reply.SavedXidCache = kv.savedXidCache[args.ConfigNum]
    reply.Err = OK
    return nil
}

func (kv *ShardKV) saveState(configNum int, oldShards []int) {
    DPrintf("Saving state for the following shards: %s", oldShards)
    kv.savedState[configNum] = make(map[string]string)
    for _, shardToSave := range oldShards {
        for key, value := range kv.state {
            if key2shard(key) == shardToSave {
                kv.savedState[configNum][key] = value
            }
        }
    }

    kv.savedXidCache[configNum] = make(map[int]map[Xid]string)
    for _, shardToSave := range oldShards {
        kv.savedXidCache[configNum][shardToSave] = make(map[Xid]string)
        for clientXid, value := range kv.xidCache[shardToSave] {
            kv.savedXidCache[configNum][shardToSave][clientXid] = value
        }
    }
    DPrintf("Finished saving state")
}

/*
We transition from oldConfig to newConfig.

This function compares the two configs and returns:
  (1) the newly assigned shards
  (2) the old shards that are to be handed off to another group
*/
func (kv *ShardKV) getNewAndOldShards(oldConfig shardmaster.Config,
                                      newConfig shardmaster.Config) ([]int, []int) {
    newShards := make([]int, 0) // shards that we are now responsible for
    oldShards := make([]int, 0) // sh    ards that we are no longer responsible for
    for shardNum, newShardGid := range newConfig.Shards {
        oldShardGid := oldConfig.Shards[shardNum]
        if newShardGid == kv.gid && oldShardGid != kv.gid {
            newShards = append(newShards, shardNum)
        } else if oldShardGid == kv.gid && newShardGid != kv.gid {
            oldShards = append(oldShards, shardNum)
        }
    }
    return newShards, oldShards
}

// tell the server to shut itself down.
// please don't change these two functions.
func (kv *ShardKV) kill() {
	atomic.StoreInt32(&kv.dead, 1)
	kv.l.Close()
	kv.px.Kill()
}

// call this to find out if the server is dead.
func (kv *ShardKV) isdead() bool {
	return atomic.LoadInt32(&kv.dead) != 0
}

// please do not change these two functions.
func (kv *ShardKV) Setunreliable(what bool) {
	if what {
		atomic.StoreInt32(&kv.unreliable, 1)
	} else {
		atomic.StoreInt32(&kv.unreliable, 0)
	}
}

func (kv *ShardKV) isunreliable() bool {
	return atomic.LoadInt32(&kv.unreliable) != 0
}

//
// Start a shardkv server.
// gid is the ID of the server's replica group.
// shardmasters[] contains the ports of the
//   servers that implement the shardmaster.
// servers[] contains the ports of the servers
//   in this replica group.
// Me is the index of this server in servers[].
//
func StartServer(gid int64, shardmasters []string,
	servers []string, me int) *ShardKV {
	gob.Register(Op{})

	kv := new(ShardKV)
	kv.me = me
	kv.gid = gid
    kv.servers = servers
    kv.name = kv.servers[kv.me]
	kv.sm = shardmaster.MakeClerk(shardmasters)
    kv.localConfig = shardmaster.Config{}
    kv.seq = 0
    kv.state = make(map[string]string)
    // [shardNum -> [Xid -> value]]
    kv.xidCache = make(map[int]map[Xid]string)
    for shardNum := 0; shardNum < shardmaster.NShards; shardNum++ {
        kv.xidCache[shardNum] = make(map[Xid]string)
    }

    // [config# -> KV pairs that need to be passed to other groups]
    kv.savedState = make(map[int]map[string]string)
    // [config# -> client state that needs to be passed to other groups]
    kv.savedXidCache = make(map[int]map[int]map[Xid]string)

	// Don't call Join().

	rpcs := rpc.NewServer()
	rpcs.Register(kv)

	kv.px = paxos.Make(servers, me, rpcs)


	os.Remove(servers[me])
	l, e := net.Listen("unix", servers[me])
	if e != nil {
		log.Fatal("listen error: ", e)
	}
	kv.l = l

	// please do not change any of the following code,
	// or do anything to subvert it.

	go func() {
		for kv.isdead() == false {
			conn, err := kv.l.Accept()
			if err == nil && kv.isdead() == false {
				if kv.isunreliable() && (rand.Int63()%1000) < 100 {
					// discard the request.
					conn.Close()
				} else if kv.isunreliable() && (rand.Int63()%1000) < 200 {
					// process the request but force discard of reply.
					c1 := conn.(*net.UnixConn)
					f, _ := c1.File()
					err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
					if err != nil {
						fmt.Printf("shutdown: %v\n", err)
					}
					go rpcs.ServeConn(conn)
				} else {
					go rpcs.ServeConn(conn)
				}
			} else if err == nil {
				conn.Close()
			}
			if err != nil && kv.isdead() == false {
				fmt.Printf("ShardKV(%v) accept: %v\n", me, err.Error())
				kv.kill()
			}
		}
	}()

	go func() {
		for kv.isdead() == false {
			kv.tick()
			time.Sleep(250 * time.Millisecond)
		}
	}()

	return kv
}
