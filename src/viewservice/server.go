package viewservice

import "net"
import "net/rpc"
import "log"
import "time"
import "sync"
import "fmt"
import "os"
import "sync/atomic"

/*
The primary in a view must always be either the primary or the backup of the
previous view. This helps ensure that the key/value service's state is
preserved. An exception: when the viewservice first starts, it should accept
any server at all as the first primary. The backup in a view can be any
server (other than the primary), or can be altogether missing if no server is
available (represented by an empty string, "").

Each key/value server should send a Ping RPC once per PingInterval
(see viewservice/common.go). The view service replies to the Ping with a
description of the current view. A Ping lets the view service know that the
key/value server is alive; informs the key/value server of the current view;
and informs the view service of the most recent view that the key/value
server knows about. If the viewservice doesn't receive a Ping from a server
for DeadPings PingIntervals, the viewservice should consider the server to
be dead. When a server re-starts after a crash, it should send one or more
Pings with an argument of zero to inform the view service that it crashed.

The view service proceeds to a new view if it hasn't received recent Pings
from both primary and backup, or if the primary or backup crashed and
restarted, or if there is no backup and there is an idle server (a server
that's been Pinging but is neither the primary nor the backup). But the view
service must not change views (i.e., return a different view to callers)
until the primary from the current view acknowledges that it is operating in
the current view (by sending a Ping with the current view number). If the
view service has not yet received an acknowledgment for the current view
from the primary of the current view, the view service should not change
views even if it thinks that the primary or backup has died. That is, the
view service may not proceed from view X to view X+1 if it has not received
a Ping(X) from the primary of view X.

The acknowledgment rule prevents the view service from getting more than
one view ahead of the key/value servers. If the view service could get
arbitrarily far ahead, then it would need a more complex design in which it
kept a history of views, allowed key/value servers to ask about old views,
and garbage-collected information about old views when appropriate. The
downside of the acknowledgement rule is that if the primary fails before it
acknowledges the view in which it is primary, then the view service cannot
ever change views again.
*/

type ViewServer struct {
	mu       sync.Mutex
	l        net.Listener
	dead     int32 // for testing
	rpccount int32 // for testing
	me       string

	viewnum			uint
	primaryAckd		bool // did the primary send an ack for the current viewnum?
	primary 		string
	backup  		string
	lastPing 		map[string]time.Time
}

//
// server Ping RPC handler.
//
func (vs *ViewServer) Ping(args *PingArgs, reply *PingReply) error {
	client := args.Me
	viewno := args.Viewnum
	
	vs.mu.Lock()
	defer vs.mu.Unlock()
	
	if viewno > vs.viewnum {
		return fmt.Errorf("Client ping's viewnum is greater than server's viewnum")
	}
	
	// always update last time we've seen this client
	vs.lastPing[client] = time.Now()
	
	if vs.primary == "" && vs.backup == "" {
		// Base case of no primary or backup
		// viewnum should be 0
		if vs.viewnum != 0 {
			return fmt.Errorf("Neither primary nor backup is assigned, but viewnum is not 0")
		}
		vs.viewnum = 1
		vs.primaryAckd = false
		vs.primary = client

	} else if client == vs.primary {
		if viewno == vs.viewnum {
			// We've received an ack for the current viewnum.
	        // (we can now advance the view state if necessary)
			vs.primaryAckd = true

		} else if viewno == 0 {
			// primary is restarted!
			//
			// need to promote the backup IFF
			// the backup exists and the primary had already ack'd
			if vs.primaryAckd && vs.backup != "" {
				vs.viewnum += 1
				vs.primaryAckd = false
				vs.primary = vs.backup
				vs.backup = ""
					
				// attempt to find a replacement for the backup
				// below it is possible that the restarted primary will
				// become the backup
				for c, lp := range vs.lastPing {
					alive := time.Since(lp) < (PingInterval * DeadPings)
					if alive && c != vs.primary {
						vs.backup = c
						break
					}
				}
			}
			
		}
		
		// Nothing to do. We already updated last time we've
		// seen this client.
		
	} else if client == vs.backup {
		
		if viewno == 0 {
			// The backup was restarted
			// However, there's nothing to do here
			// If we were to find a replacement backup, we might
			// as well select client (the backup that restarted)
			//
			// It will be up to the restarted backup server to
			// make sure to communicate to the primary that it
			// needs to reinitialise its state.
			
		}
		
		// Nothing to do. We already updated last time we've
		// seen this client.
		
	} else {
		
		// Client is not primary or standby.
		//
		// - We know that the primary is already assigned (from base case above).
		// - If the backup is not assigned, we can set this client
		//   as the backup IFF the primary has ack'd the current view number.
		//     - If the primary hasn't ack'd and backup is empty, then
		//       eventually this backup will be filled by future pings
		// - If backup is already assigned, we do nothing. We've already
		//   added this server to lastPing, so we treat it as a reserve which
		//   can potentially become the backup later in tick().
		
		if vs.backup == "" && vs.primaryAckd {
			// set client as backup
			vs.viewnum += 1
			vs.primaryAckd = false
			vs.backup = client
		}
		
	}

	reply.View.Viewnum = vs.viewnum;
	reply.View.Primary = vs.primary;
	reply.View.Backup = vs.backup;
	return nil
}

//
// server Get() RPC handler.
//
func (vs *ViewServer) Get(args *GetArgs, reply *GetReply) error {

	vs.mu.Lock()
	defer vs.mu.Unlock()
	reply.View.Viewnum = vs.viewnum
	reply.View.Primary = vs.primary
	reply.View.Backup = vs.backup
	return nil
}


//
// tick() is called once per PingInterval; it should notice
// if servers have died or recovered, and change the view
// accordingly.
//
func (vs *ViewServer) tick() {

	vs.mu.Lock()
	defer vs.mu.Unlock()
	
	// iterate through last pings and find dead servers
	for client, lastPing := range vs.lastPing {
		dead := time.Since(lastPing) > (PingInterval * DeadPings)
		
		if dead {
			if client == vs.primary {
				// primary has died!
				// attempt to replace it with a backup IFF
				// the primary had ack'd and a backup exists
				//
				// remember that we can't change the view unless
				// the primary was ack'd
				if vs.primaryAckd && vs.backup != "" {
					vs.viewnum += 1
					vs.primaryAckd = false
					vs.primary = vs.backup
					vs.backup = ""
					delete(vs.lastPing, client)
					
					// attempt to find a replacement for the backup
					for c, lp := range vs.lastPing {
						alive := time.Since(lp) < (PingInterval * DeadPings)
						if alive && c != vs.primary {
							vs.backup = c
							break
						}
					}
				}
				
			} else if client == vs.backup {
				// we need to remove the backup, but
				// can only advance view after primary has ack'd
				if vs.primaryAckd {
					vs.viewnum += 1
					vs.primaryAckd = false
					vs.backup = ""
					delete(vs.lastPing, client)
					
					// attempt to find a replacement for the backup
					for c, lp := range vs.lastPing {
						alive := time.Since(lp) < (PingInterval * DeadPings)
						if alive && c != vs.primary {
							vs.backup = c
							break
						}
					}
				}
				
			} else {
				// remove the backup candidate
				delete(vs.lastPing, client)
			}
		}
	}
}

//
// tell the server to shut itself down.
// for testing.
// please don't change these two functions.
//
func (vs *ViewServer) Kill() {
	atomic.StoreInt32(&vs.dead, 1)
	vs.l.Close()
}

//
// has this server been asked to shut down?
//
func (vs *ViewServer) isdead() bool {
	return atomic.LoadInt32(&vs.dead) != 0
}

// please don't change this function.
func (vs *ViewServer) GetRPCCount() int32 {
	return atomic.LoadInt32(&vs.rpccount)
}

func StartServer(me string) *ViewServer {
	vs := new(ViewServer)
	vs.me = me
	vs.viewnum = 0
	vs.primary = ""
	vs.backup = ""
	vs.lastPing = make(map[string]time.Time)
	vs.primaryAckd = false

	// tell net/rpc about our RPC server and handlers.
	rpcs := rpc.NewServer()
	rpcs.Register(vs)

	// prepare to receive connections from clients.
	// change "unix" to "tcp" to use over a network.
	os.Remove(vs.me) // only needed for "unix"
	l, e := net.Listen("unix", vs.me)
	if e != nil {
		log.Fatal("listen error: ", e)
	}
	vs.l = l

	// please don't change any of the following code,
	// or do anything to subvert it.

	// create a thread to accept RPC connections from clients.
	go func() {
		for vs.isdead() == false {
			conn, err := vs.l.Accept()
			if err == nil && vs.isdead() == false {
				atomic.AddInt32(&vs.rpccount, 1)
				go rpcs.ServeConn(conn)
			} else if err == nil {
				conn.Close()
			}
			if err != nil && vs.isdead() == false {
				fmt.Printf("ViewServer(%v) accept: %v\n", me, err.Error())
				vs.Kill()
			}
		}
	}()

	// create a thread to call tick() periodically.
	go func() {
		for vs.isdead() == false {
			vs.tick()
			time.Sleep(PingInterval)
		}
	}()

	return vs
}
