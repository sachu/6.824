package mapreduce

import "container/list"
import "fmt"
import "time"


type WorkerInfo struct {
	address string
	// You can add definitions here.
}


// Clean up all workers by sending a Shutdown RPC to each one of them
// Collect the number of jobs each work has performed.
func (mr *MapReduce) KillWorkers() *list.List {
	l := list.New()
	for _, w := range mr.Workers {
		DPrintf("DoWork: shutdown %s\n", w.address)
		args := &ShutdownArgs{}
		var reply ShutdownReply
		ok := call(w.address, "Worker.Shutdown", args, &reply)
		if ok == false {
			fmt.Printf("DoWork: RPC %s shutdown error\n", w.address)
		} else {
			l.PushBack(reply.Njobs)
		}
	}
	return l
}

/*
Returns
	-2	:: all jobs complete
	-1	:: all jobs assigned
	>=0	:: jid of first unassigned job found
*/
func (mr *MapReduce) getUnassignedMapJobId() int {
	jid := -2
	mr.mapMu.Lock()
	defer mr.mapMu.Unlock()
	for i := 0; i < mr.nMap; i++ {
		if mr.mapJobs[i] != complete {
			jid = -1
		}
		if mr.mapJobs[i] == unassigned {
			return i
		}
	}
	return jid;
}

func (mr *MapReduce) getUnassignedReduceJobId() int {
	jid := -2
	mr.reduceMu.Lock()
	defer mr.reduceMu.Unlock()
	for i := 0; i < mr.nReduce; i++ {
		if mr.reduceJobs[i] != complete {
			jid = -1
		}
		if mr.reduceJobs[i] == unassigned {
			return i
		}
	}
	return jid;
}

func (mr *MapReduce) runMapPhase() {
	for i := 0; i < mr.nMap; i++ {
		mr.mapJobs[i] = unassigned
	}
	
	for {
		jid := mr.getUnassignedMapJobId()
		if jid == -2 {
			return
		} else if jid == -1 {
			// all jobs have been assigned, sleep for a bit
			time.Sleep(100 * time.Millisecond)
		} else {
			// assign a job once we have an available worker
			worker := <-mr.availableWorkers
			mr.mapMu.Lock()
			mr.mapJobs[jid] = processing
			mr.mapMu.Unlock()
			go func() {
				// prep the args and reply
				args := &DoJobArgs{}
				args.File = mr.file
				args.Operation = Map
				args.JobNumber = jid
				args.NumOtherPhase = mr.nReduce
				reply := &DoJobReply{}
				
				call(worker, "Worker.DoJob", args, reply)
				
				mr.mapMu.Lock()
				if reply.OK {
					mr.mapJobs[jid] = complete
				} else {
					mr.mapJobs[jid] = unassigned
				}
				mr.mapMu.Unlock()
				mr.availableWorkers <- worker // signal
			}()
		}
	}
}

func (mr *MapReduce) runReducePhase() {
	for i := 0; i < mr.nReduce; i++ {
		mr.reduceJobs[i] = unassigned
	}
	
	for {
		jid := mr.getUnassignedReduceJobId()
		if jid == -2 {
			return
		} else if jid == -1 {
			// all jobs have been assigned, sleep for a bit
			time.Sleep(100 * time.Millisecond)
		} else {
			// assign a job once we have an available worker
			worker := <-mr.availableWorkers
			mr.reduceMu.Lock()
			mr.reduceJobs[jid] = processing
			mr.reduceMu.Unlock()
			go func() {
				// prep the args and reply
				args := &DoJobArgs{}
				args.File = mr.file
				args.Operation = Reduce
				args.JobNumber = jid
				args.NumOtherPhase = mr.nMap
				reply := &DoJobReply{}
				
				call(worker, "Worker.DoJob", args, reply)
				
				mr.reduceMu.Lock()
				if reply.OK {
					mr.reduceJobs[jid] = complete
				} else {
					mr.reduceJobs[jid] = unassigned
				}
				mr.reduceMu.Unlock()
				mr.availableWorkers <- worker // signal
			}()
		}
	}
}

func (mr *MapReduce) RunMaster() *list.List {
	// Your code here
	fmt.Println("Starting the master")
	
	// run map phase
	mr.runMapPhase();
	
	// run reduce phase
	mr.runReducePhase();
	
	// mappers and reducers need to complete before we proceed
	fmt.Println("Shutting down the master and workers")
	return mr.KillWorkers()
}
